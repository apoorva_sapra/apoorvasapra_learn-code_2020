from dataclasses import dataclass

@dataclass
class ImqProtocol:
    data: str
    format: str = "json"
    version: str = "1.0"
