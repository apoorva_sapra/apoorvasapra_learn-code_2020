from dataclasses import dataclass
from imqProtocol import *

@dataclass
class Response(ImqProtocol):
    status: str = "//sent"