from dataclasses import dataclass
from imqprotocol import *

@dataclass
class Request(ImqProtocol):
    requestType: str = "connect"
