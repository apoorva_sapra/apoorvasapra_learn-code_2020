import socket
import os
from _thread import start_new_thread 
from constants import Constants
from storage import Storage

class Server:

    ServerSocket = socket.socket()
    constants = Constants()
    storage = Storage()

    def createServerSocket(self):
        return socket.socket()

    def bindServerSocket(self,serverSocket,host,port):
        try:
            serverSocket.bind((host, port))
        except socket.error as errorMessage:
            print(str(errorMessage))

    def echoToClient(self,data,clientSocket):
        reply = self.constants.serverReplyMessage + data
        clientSocket.sendall(str.encode(reply))

    def storeClientRequest(self,clientSocket,clientName):
        # try:
            while True:
                data = clientSocket.recv(self.constants.buffer).decode('utf-8')
                self.storage.storeClientRequestInDatabase(clientName,data)
                if data.strip().lower() == self.constants.terminateConnection:
                    break
                else:
                    self.echoToClient(data,clientSocket)
            clientSocket.close()
        # except:
        #     print("Client data could not be saved.")

    def connectToClient(self,serverSocket):
        while True:
            clientSocket, address = serverSocket.accept()
            clientName = address[0] + '__' + str(address[1])
            print('Connected to',clientName)
            start_new_thread(self.storeClientRequest, (clientSocket,clientName))

def main():
    constants = Constants()
    host = constants.host
    port = constants.port
    server=Server()
    serverSocket=server.createServerSocket()
    server.bindServerSocket(serverSocket,host,port)
    print('Server waiting for a Connection..')
    serverSocket.listen(5)
    server.connectToClient(serverSocket)
    serverSocket.close()


main()
