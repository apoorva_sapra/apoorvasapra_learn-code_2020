from constants import Constants
from datetime import datetime
import mysql.connector
from DatabaseQueries import *
from DatabaseConstants import * 

class Storage:
    constants = Constants()
    def storeClientRequestInFile(self,clientSocket,fileName,data):
        try:
            with open(fileName, 'a') as file: 
                currentTime=datetime.now().strftime("%H:%M:%S")
                file.write('Client sent '+data + ' at: ' + currentTime +'\n')
        except EOFError as errorMessage:
            print(errorMessage)

    def connectWithDatabase(self):
        databaseConnection = mysql.connector.connect(
                host = databaseHost,
                user = databaseUser,
                password = databasePassword,
                database = database
        )
        return databaseConnection

    def getCursor(self,databaseConnection):
        cursor = databaseConnection.cursor()
        return cursor

    def storeClientRequestInDatabase(self,clientName,data): 
        databaseConnection = self.connectWithDatabase()
        cursor = self.getCursor(databaseConnection)
        self.insertIntoReferenceTable(clientName,cursor,databaseConnection)
        isTableExists = self.checkTableExists(cursor,clientName)
        if not isTableExists:
            self.createClientTable(clientName, cursor)
        self.insertDataInClientTable(clientName,cursor,data,databaseConnection) 

    def checkTableExists(self, cursor, tablename):
        cursor.execute(checkClientTableExistsQuery)
        if len(cursor.fetchall()) > 0:
            return True
        print("table new")
        return False

    def createReferenceTable(self,cursor,databaseConnection):
        try:
            cursor.execute(createReferenceTable)
            databaseConnection.commit()
        except:
            print("Reference table could not be created")

    def insertIntoReferenceTable(self,clientName,cursor,databaseConnection):
        self.createReferenceTable(cursor,databaseConnection)
        try:
            values=[clientName]
            cursor.execute(insertQueryForReferenceTable,values)
        except:
            print("Client information could not be stored in reference table")
        databaseConnection.commit()

    def createClientTable(self, clientName, cursor):
        print(clientName)
        cursor.execute(createClientTableCommand)
    
    def getClientId(self, clientName, cursor):
        value = [clientName]
        try:
            cursor.execute(getClientIdQuery,value)
            clientId = cursor.fetchall()[0][0]
            return clientId
        except:
            print("Client id could not be retrieved")
            
    def insertDataInClientTable(self,clientName,cursor,data,databaseConnection):
        now = datetime.utcnow()
        try:         
            currentTime=now.strftime('%Y-%m-%d %H:%M:%S')
            clientId=self.getClientId(clientName,cursor)
            values=(currentTime,data,clientId)
            cursor.execute(insertQueryForClientTable,values)
        except:
            print("Client requests could not be saved.")
        databaseConnection.commit()
        


