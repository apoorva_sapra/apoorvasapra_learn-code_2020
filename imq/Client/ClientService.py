import socket
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('../')
from Client.AppConstants import *
from Client.ClientMessageController import *

class ClientService:    
    host = HOST
    port = PORT
    clientMessageController = ClientMessageController()
    clientRole=""
    topicName=""
    clientSocket=""

    def connectClientToServer(self):
        try:
            self.clientSocket=socket.socket()
            self.clientSocket.connect((self.host, self.port))
            print("Connected to server")
        except:
            print("Could not connect to server")
    
    def getWelcomeMessageFromServer(self):
        jsonWelcomeResponse = self.clientMessageController.receiveMessageFromServer(self.clientSocket)
        print(jsonWelcomeResponse['data'])

    def displayHelpString(self):
        print(HELP_STRING)

    def communicateWithServer(self):
        try:   
            clientName = input("Please provide your unique username to connect. \n")  
            self.clientMessageController.sendMessageToServer(self.clientSocket,clientName)
            self.displayHelpString()
            while True:
                #imq connect "books"
                #imq publish -m "message"
                #imq subscribe
                inputCommand = input(">>> ")
                if inputCommand.lower() == TERMINATE:
                    self.clientMessageController.sendMessageToServer(self.clientSocket,inputCommand)              
                    break
                self.implementClientAction(inputCommand)
                # self.clientMessageController.sendMessageToServer(clientSocket,inputCommand)              
        except:
            print("Connection broken.")

    def implementClientAction(self,inputCommand):
        clientAction = self.getClientAction(inputCommand)
        # if clientAction and not clientAction.isspace():
        #     self.clientMessageController.sendMessageToServer(
        #         self.clientSocket, clientAction)

        if clientAction == CONNECT:
            self.implementConnectAction(inputCommand)  
        
        if clientAction == ADD:
            self.implementAddAction(inputCommand) 

        if ((clientAction == PUBLISH or clientAction == SUBSCRIBE) and (self.topicName == "")):
            print("Please connect to topic before publishing or subscribing.")
        
        if ((clientAction == PUBLISH) and (not self.topicName.isspace()) and (self.topicName)):
            self.implementPublishAction(inputCommand)

        if clientAction == SUBSCRIBE and self.topicName != "":
            self.implementSubscribeAction()

        if clientAction == MANUAL:
            print(HELP_STRING)


    def implementConnectAction(self,inputCommand):
        self.topicName = self.getTopic(inputCommand)
        if self.topicName and not self.topicName.isspace():
            print(self.topicName)
            self.sendTopicToConnectToServer(self.topicName)
            print(self.clientMessageController.receiveMessageFromServer(self.clientSocket)['data'])
        else:
            print(INVALID_COMMAND)

    def implementAddAction(self,inputCommand):
        self.topicName = self.getTopic(inputCommand)
        if self.topicName and not self.topicName.isspace():
            print(self.topicName)
            self.sendTopicToAddToServer(self.topicName)
            print(self.clientMessageController.receiveMessageFromServer(self.clientSocket)['data'])
        else:
            print(INVALID_COMMAND)

    def implementPublishAction(self,inputCommand):
        messageToPublish = self.getMessageToPublish(inputCommand)
        if messageToPublish and not messageToPublish.isspace():
            self.sendMessageToPublishToServer(messageToPublish)
        else:
            print(INVALID_COMMAND)

    def implementSubscribeAction(self):
        self.clientMessageController.sendMessageToServer(
            self.clientSocket,message='pull', request= SUBSCRIBE)
        subscriberJsonResponse=self.clientMessageController.receiveMessageFromServer(self.clientSocket)
        self.displaySubscribedMessages(subscriberJsonResponse['data'])
    
    def displaySubscribedMessages(self,subscriberJsonResponse):
        for eachResponse in subscriberJsonResponse:
            print(eachResponse[0])
        
    def getClientAction(self,inputCommand):
        try:
            if inputCommand.split(' ')[0] != IMQ:
                raise Exception
            else: 
                clientAction = inputCommand.split(' ')[1]
                if clientAction in CLIENT_ACTIONS and not clientAction.isspace():
                    return clientAction
                raise Exception
        except:
            print(INVALID_COMMAND)
            

    def getTopic(self,inputCommand):
        try:
            topicName = inputCommand.split()[2]
            return topicName
        except:
            print(INVALID_COMMAND)

    def addTopic(self,topicName):
        pass
    
    def getMessageToPublish(self,inputCommand):
        try:
            messageToPublish = inputCommand.split('-m')[1].split("\"")[1]
            return messageToPublish
        except:
            print("Message not provided") 

    def sendMessageToPublishToServer(self,messageToPublish):
        self.clientMessageController.sendMessageToServer(
                self.clientSocket, messageToPublish, request= PUBLISH)
                #msg rec by server published ack

    def sendTopicToConnectToServer(self,topicName):
        self.clientMessageController.sendMessageToServer(
            self.clientSocket, topicName)
        print("topic sent")

    def sendTopicToAddToServer(self,topicName):
        self.clientMessageController.sendMessageToServer(
            self.clientSocket, topicName, request=ADD)
        print("topic sent")

