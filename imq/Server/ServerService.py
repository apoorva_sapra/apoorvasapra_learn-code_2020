from Storage.DatabaseHandler import *
from Server.PublisherHandler import *
from Server.SubscriberHandler import *
from Server.ServerMessageController import *
from Server.AppConstants import *
import socket
from _thread import start_new_thread
import sys
import os
sys.path.append('../')
sys.path.append(os.path.dirname(os.path.abspath(__file__)))


class ServerService:

    serverSocket = socket.socket()
    clientSocket = ""
    clientName = ""
    storage = DatabaseHandler()
    serverMessageController = ServerMessageController()
    subscriberHandler = SubscriberHandler()
    publisherHandler = PublisherHandler()
    __topicName = ""

    def createServerSocket(self):
        return socket.socket()

    def createClientSocket(self):
        self.clientSocket, address = self.serverSocket.accept()

    def bindServerSocket(self, host, port):
        try:
            self.serverSocket.bind((host, port))
        except socket.error as errorMessage:
            print(str(errorMessage))

    def startServerListener(self):
        self.serverSocket.listen(10)

    def acceptConnectionFromClient(self):
        return self.serverSocket.accept()

    def sendWelcomeMessageToClient(self):
        self.serverMessageController.sendMessageToClient(
            self.clientSocket, WELCOME)

    def createTablesInDatabase(self):
        self.storage.connectWithDatabase()
        self.storage.createDatabaseTables()

    def AddTopicsInDatabase(self):
        self.storage.AddTopicsInTopicTable()

    def AddRolesInDatabase(self):
        self.storage.AddRolesInRoleTable()

    def startNewClientThread(self):
        while True:
            print("started new client thread")
            self.createClientSocket()
            self.sendWelcomeMessageToClient()
            self.clientName = self.serverMessageController.receiveMessageFromClient(
                self.clientSocket)[DATA]
            print(CONNECTED_TO, self.clientName)
            start_new_thread(self.startClientThread, ())
        self.serverSocket.close()

    def startClientThread(self):
        try:
            while True:
                self.storage.insertIntoReferenceTable(self.clientName)
                # send avaialble topics
                # self.storage.showAvailableTopics() query
                # self.storage.storeClientRequestInDatabase(clientName,data)
                clientJsonResponse = self.serverMessageController.receiveMessageFromClient(
                    self.clientSocket)
                if clientJsonResponse[DATA].lower() == TERMINATE:
                    print(self.clientName+" disconnected")
                    break
                self.handleClientRequest(clientJsonResponse)
            self.clientSocket.close()
        except:
            print("Something went wrong.")

    def handleClientRequest(self, clientJsonResponse):
        if clientJsonResponse[REQUEST_TYPE] == CONNECT:
            topicEnteredByClient = clientJsonResponse[DATA].strip().lower()
            if not self.storage.checkItemNotExistsInTable(topicEnteredByClient, TOPIC):
                self.__topicName = clientJsonResponse[DATA]
                print("topic = "+self.__topicName)
                self.serverMessageController.sendMessageToClient(
                    self.clientSocket, CONNECTION_SUCCESSFULL)
            else:
                self.serverMessageController.sendMessageToClient(
                    self.clientSocket, TOPIC_UNAVAILABLE)

        if clientJsonResponse[REQUEST_TYPE] == PUBLISH:
            messageToPublish = clientJsonResponse[DATA]
            print(clientJsonResponse)
            print("topic = "+self.__topicName)
            print("message = " + messageToPublish)
            self.publisherHandler.handlePublishRequest(
                self.clientSocket, messageToPublish, self.__topicName)

        if clientJsonResponse[REQUEST_TYPE] == SUBSCRIBE:
            print(clientJsonResponse)
            self.subscriberHandler.handleSubscribeRequest(
                self.clientSocket, self.clientName, self.__topicName)

        if clientJsonResponse[REQUEST_TYPE] == ADD:
            print(clientJsonResponse)
            self.AddTopicInTopicTable(clientJsonResponse[DATA].lower())

    def AddTopicInTopicTable(self, topicName):
        if self.storage.AddNewTopicInTopicTable(topicName):
            print("Added new topic")
            self.serverMessageController.sendMessageToClient(
                self.clientSocket, NEW_TOPIC_ADDED)
        else:
            self.serverMessageController.sendMessageToClient(
                self.clientSocket, TOPIC_ALREADY_EXISTS)
