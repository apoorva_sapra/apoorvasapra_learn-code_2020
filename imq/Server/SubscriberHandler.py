import sys
import os
sys.path.append('../')
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from Server.AppConstants import *
from Storage.DatabaseHandler import * 
from Server.ServerMessageController import *

class SubscriberHandler:
    __clientId = 0
    __topicId = 0
    __messages =[]
    __clienSocket=""
    databaseHandler = DatabaseHandler()
    serverMessageController= ServerMessageController()

    def handleSubscribeRequest(self,clientSocket,clientName,topicName):
        self.databaseHandler.connectWithDatabase()
        self.__clientId = self.databaseHandler.getClientId(clientName)
        self.__topicId=self.databaseHandler.getTopicId(topicName)
        self.__clientSocket=clientSocket
        self.fetchDataFromMessageTable()

    def fetchDataFromMessageTable(self):
        lastSeenMessageId : int
        if not self.databaseHandler.checkClientAndTopicExistInMessageMapping(self.__clientId,self.__topicId):
            self.__messages = self.databaseHandler.getMessagesFromClientMessageTable(self.__topicId)
            # self.__messageList = self.getMessagesFromQueue()
            if self.__messages:
                lastSeenMessageId = self.getLastSeenMessageId()
                self.databaseHandler.addDataInMessageMappingTable(self.__clientId, self.__topicId, lastSeenMessageId)
        else:
            print("else")
            lastSeenMessageId = self.databaseHandler.getLastSeenMessageIdFromMessageMappingTable(self.__topicId, self.__clientId)
            self.__messages = self.databaseHandler.getUnseenMessages(lastSeenMessageId,self.__topicId)
            # self.__messageList = self.getNewMessagesFromQueue(serialNumberOfLastMessage)
            if self.__messages:
                lastSeenMessageId = self.getLastSeenMessageId()
                self.databaseHandler.updateLastSeenMessageId(lastSeenMessageId,self.__clientId,self.__topicId)
        self.sendMessagesToClient()

    def getLastSeenMessageId(self):
        lengthOfMessages=len(self.__messages)
        return self.__messages[lengthOfMessages-1][1]
        
    def sendMessagesToClient(self):
        self.serverMessageController.sendMessageToClient(self.__clientSocket, self.__messages)

