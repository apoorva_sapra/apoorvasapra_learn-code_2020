from Storage.DatabaseHandler import *
from Server.ServerMessageController import *
from Server.AppConstants import *
import sys
import os
sys.path.append('../')
sys.path.append(os.path.dirname(os.path.abspath(__file__)))


class PublisherHandler:
    databaseHandler = DatabaseHandler()
    serverMessageController = ServerMessageController()

    def handlePublishRequest(self, clientSocket, messageToPublish, topicName):
        self.databaseHandler.insertDataInClientMessageTable(
            messageToPublish, topicName)
        self.serverMessageController.sendMessageToClient(clientSocket,MESSAGE_PUBLISHED)
