import socket
from _thread import start_new_thread 
import sys
import os
sys.path.append('../')
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from Server.AppConstants import *
from Storage.DatabaseHandler import * 
from Server.ServerMessageController import *
from Server.ServerService import *

def startServer():
    host = HOST
    port = PORT
    # try:
    server=ServerService()
    server.bindServerSocket(host,port)
    server.startServerListener()
    print('Server waiting for a Connection..')
    server.createTablesInDatabase()
    server.AddTopicsInDatabase()
    server.AddRolesInDatabase()
    # think of name for startNewClientThread and iske andar vala
    server.startNewClientThread()
    # except:
    #     print("Connection could not be established.")

startServer()