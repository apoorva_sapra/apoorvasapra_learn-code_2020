import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('../')
from ExceptionConstants import *

class ClientException(Exception):
    pass

class InvalidCommandException(ClientException):
    value=""
    def __init__(self):
        self.value = INVALID_COMMAND
