from dataclasses import dataclass
from Protocol.imqProtocol import *
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

# not required
@dataclass
class Response(ImqProtocol):
    status: str = RESPONSE_MESSAGE