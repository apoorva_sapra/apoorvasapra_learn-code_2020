using Xunit;
public class Tests
{
    [Fact]
    public void getCountOfSamePositiveDivisorsPass()
    {
        int inputNumber = 15;
        int actualValue = 2;
        int expectedValue = assignment8.Program.getCountOfSamePositiveDivisors(inputNumber);
        Assert.Equal(actualValue, expectedValue);
    }

    [Fact]
    public void getCountOfSamePositiveDivisorsFailure()
    {
        int inputNumber = 3;
        int actualValue = 2;
        int expectedValue = assignment8.Program.getCountOfSamePositiveDivisors(inputNumber);
        Assert.NotEqual(actualValue, expectedValue);
    }
}