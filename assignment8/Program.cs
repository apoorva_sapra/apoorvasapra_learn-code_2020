﻿using System.Linq;
using System;
using System.Collections.Generic;

namespace assignment8
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter number of test cases");
            int numberOfTestCases = Int32.Parse(Console.ReadLine());
            int[] userInput = new int[numberOfTestCases];
            List<int> result = new List<int>();
            try{
            userInput = getInput(numberOfTestCases);
            }
            catch(Exception){
                Console.WriteLine("Invalid number of testcases");
            }
            foreach (int value in userInput)
            {
                    result.Add(getCountOfSamePositiveDivisors(value));
            }
            
            displayResult(result);
        }

        public static int[] getInput(int numberOfTestCases)
        {
            int[] userInput = new int[numberOfTestCases];
            Console.WriteLine("Enter values");
            if (numberOfTestCases > 0)
            {
                for (int index = 0; index < numberOfTestCases; index++)
                {
                    userInput[index] = Int32.Parse(Console.ReadLine());
                }
            }
            return userInput;
        }
        
        public static int getCountOfSamePositiveDivisors(int inputNumber)
        {
            int countFactors = 0;

            for (int number = 2; number < inputNumber; number++)
            {
                List<int> firstNumberFactorsList = new List<int>();
                List<int> nextNumberFactorsList = new List<int>();
                for (int factor = 1; factor < inputNumber + 1; factor++)
                {
                    if (number % factor == 0)
                    {
                        firstNumberFactorsList.Add(factor);
                    }
                    if ((number + 1) % factor == 0)
                    {
                        nextNumberFactorsList.Add(factor);
                    }
                }

                if(firstNumberFactorsList.Count==nextNumberFactorsList.Count)
                {
                    countFactors++;
                }
            }
            return countFactors;
        }

        public static void displayResult(List<int> result){
            for(int index=0; index<result.Count; index++){
                Console.WriteLine(result[index]);
            }
        }
    }
}
