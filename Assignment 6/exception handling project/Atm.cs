using System.ComponentModel.Design.Serialization;
using System;
using System.Net;
namespace exception_handling_project
{
    public class Atm
    {
        int pinAttempts=0;
        string cardStatus = "notBlocked";
        static Services services=new Services();
        public Atm(){
            Console.WriteLine("Welcome to IntimeTec ATM!");
            try{
                this.enterPin();
            }
            catch(FormatException){
                Console.WriteLine("Pin can only be a number");
            }
            catch(Exception exception){
                Console.WriteLine(exception.Message);
             }
        }

    public void enterPin()
    {
        enterPin:
        if(cardStatus =="notBlocked")
        {
            if (pinAttempts == 3)
                {
                    cardStatus = "Blocked";
                    Console.WriteLine("Card blocked due to too many attempts.");
                }
            else
                {
                    Console.WriteLine("Please enter your pin: ");
                    int pin=Convert.ToInt32(Console.ReadLine());
                    bool isPinValid = services.checkPin(pin);
                        if (!isPinValid)
                            {
                                pinAttempts = this.increasePinAttempts(pinAttempts);
                                Console.WriteLine("Invalid pin!");
                                goto enterPin;
                            }
                        else
                            {
                                getUsersChoice();
                            }
                }

        }

    }
    public int increasePinAttempts(int pinTries)
    {
        pinTries += 1;
        return pinTries;
    }

    static public void getUsersChoice()
        {
                while(true){
                Console.WriteLine("\nEnter your choice: \n Press 1 to check balance \n Press 2 for deposit \n Press 3 for withdraw \n Press 4 to exit");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        {
                            services.checkBalance();
                            break;
                        }
                    case "2":
                        {
                            services.deposit();
                            break;
                        }
                    case "3":
                        {
                            services.withdraw();
                            break;
                        }
                    case "4":
                        {
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Enter Valid Choice");
                            break;
                        }
                }
                if(choice=="4"){
                    Console.WriteLine("Have a nice day :)");
                    break;
                    }
                } 
    }
  }
}