using System;
namespace exception_handling_project
{
    public class Services
    {
        double availableCash = 90;
        Customer customer = new Customer();
        Transactions transactions = new Transactions();
        public bool checkPin(int pin){
             if (customer.pin == pin)
                {
                   return true;
                }
            return false;
        }
        
        public void checkBalance()
        {
            Console.WriteLine($"hi {customer.name}! Your current balance is Rs.{customer.balance} ");
        }

        public void withdraw()
        {
            try{  
            if(availableCash<100)
                    {
                        throw new Exception("Insufficient cash in ATM! Please try some other option :)");
                    }
            else
            {
                Console.WriteLine("Enter amount to withdraw");
                double amount = Convert.ToDouble(Console.ReadLine());
                    if (availableCash < amount)
                    {
                        throw new Exception($"Sorry current withdrawl limit for atm is Rs {availableCash} only");
                    }
                    else if (amount>20000)
                    {
                    throw new Exception("Sorry Withdrawl limit 20,000 only!");
                    }
                    else
                    {
                        transactions.withdraw(customer,amount);
                        availableCash-=amount;
                    }
                }
            }
            catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                    }  
            }
        public void deposit()
        {
            try{
                Console.WriteLine("Enter amount to deposit");
                double amount = Convert.ToDouble(Console.ReadLine());
                transactions.deposit(customer, amount);
                availableCash+=amount;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);

            }
        }
        
    }
}