﻿using System;
using System.Net;
using System.Net.Sockets;

namespace exception_handling_project
{
    class Program
    {
        static void Main(string[] args)
        {
            try{
                Server server = new Server();
                server.startServer();
                Atm atm = new Atm();
            }
            catch(Exception){
                Console.WriteLine("Unable to connect to server.");
            }
            
        }
    }
}
