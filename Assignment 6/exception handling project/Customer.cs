using System.Collections.Generic;
namespace exception_handling_project
{
    public class Customer{

        public string name { get; set; }
        public int cardNumber { get; set; }
        public int pin { get; set; }
        public double balance { get; set; }

        public Customer()
        {
            this.name = "Apoorva";
            this.cardNumber = 12345678;
            this.pin = 1111;
            this.balance = 10000;
        }
    }
}