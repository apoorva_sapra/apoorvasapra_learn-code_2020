using System;

namespace exception_handling_project
{
    public class Transactions
    {
        public void withdraw(Customer customer,double amount)
        {
                if (amount > customer.balance)
                {
                    Console.WriteLine("Sorry you don't have enough balance.");
                }
                else
                {
                    customer.balance -= amount;
                    Console.WriteLine($"Successfully withdrawn Rs.{amount}");
                }           
        }
        public void deposit(Customer customer, double amount)
        {
              customer.balance += amount;
                Console.WriteLine($"hi {customer.name}! Your current balance is now Rs.{customer.balance},you have added Rs.{amount} ");
        }
    }
}