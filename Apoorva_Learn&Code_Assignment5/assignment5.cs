using System;

namespace assignment5
{
interface ICharacter{
    string name{get;set;}
    int age{get;set;}
    string gender{get;set;}
}

public class Person: ICharacter
{
    public string name{get;set;}
    public string gender{get;set;}
    public int age{get;set;}

    public Person(string name)
        {
            this.name=name;
        }
    public void giveFood()
        {
            Console.WriteLine("Food given");
        }
}

 public class Animal: ICharacter
{
    public string  name{get;set;}
    public int age{get;set;}
    public string gender{get;set;}
    public Animal(string name)
        {
            this.name=name;
        }

    public void getFood()
        {
        Person person = new Person("name1");
        person.giveFood();
        Console.WriteLine("Had food");
        }
}
}
