import requests
import json
import sys

postCount = 1


def GetBlogName():
    return input("Enter Blog Name :")


def GetPostRange():
    return input("Enter Post Range:")


def GetNumberOfPostsAndStartValue():
    rangeList = GetPostRange().split("-")
    try:
        numberOfPosts = int(rangeList[1])-int(rangeList[0]) + 1
    except(ValueError, IndexError):
        print("Give Proper Range")
        sys.exit(1)
    start = int(rangeList[0])-1
    return numberOfPosts, start


def GetDataFromUrl(url):
    try:
        response = requests.get(url)
        print(response)
        # removing extra text at the start and end of json object
        result = response.text[22:-2]
        parsedJsonData = json.loads(result)
        return parsedJsonData
    except():
        print("\nGive proper name")
        sys.exit(1)


def DisplayBasicInformationOfBlog(apiData):
    print("Name:", apiData["tumblelog"]["name"])
    print("Total:", apiData["posts-total"])
    print("Title:", apiData["tumblelog"]["title"])
    print("Description:", apiData["tumblelog"]["description"])


def DisplayUrlsOfPost(apiData):
    global postCount
    for post in apiData["posts"]:
        print(str(postCount)+". "+post["photo-url-1280"])
        if(post["photos"] == 0):
            pass
        else:
            for item in post["photos"]:
                print("      "+item["photo-url-1280"])
            postCount += 1


def DisplayUrls(url):
    jsonData = GetDataFromUrl(url)
    DisplayUrlsOfPost(jsonData)


def GetUrl(blogName, startValue, posts):
    return(
      "https://{}.tumblr.com/api/read/json?type=photo&num={}&start={}"
      .format(blogName, posts, startValue))


def GetUrlOfPosts():
    blogName = GetBlogName()
    numberOfPosts, start = GetNumberOfPostsAndStartValue()
    url = GetUrl(blogName, start, numberOfPosts)
    jsonData = GetDataFromUrl(url)
    DisplayBasicInformationOfBlog(jsonData)
    while numberOfPosts > 50:
        url = GetUrl(blogName, start, 50)
        DisplayUrls(url)
        numberOfPosts -= 50
        start += 50
    url = GetUrl(blogName, start, numberOfPosts)
    DisplayUrls(url)


def main():
    GetUrlOfPosts()


main()
