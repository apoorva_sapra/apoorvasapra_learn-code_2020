using System;
namespace assignment1
{
    //Encapsulation
    public class Watches
    {
        private int manufacturingNumber{get;set;}
        public string brand{get;set;}
        public string model{get;set;}
        public string dial{get;set;}
        public string band{get;set;}
        public void showtime(){
            Console.WriteLine("current time");
        }
    }
}