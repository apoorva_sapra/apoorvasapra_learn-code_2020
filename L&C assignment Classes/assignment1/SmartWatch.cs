using System;
namespace assignment1
{
    //Encapsulation
    //inheritance
    public class SmartWatch:Watches
    {
        public void trackHeartRate(){
             Console.WriteLine("Trach your heart rate");
        }
        public void measureSteps(){
             Console.WriteLine("Measure steps via watch");
        }
        public void plugInToCharge(){
            Console.WriteLine("Plug in the watch to charge");
        }
        //Polyorphism
        public void setAlarm(string time){
            Console.WriteLine("Alarm set for time" + time);
        }
        public void setAlarm(string time, string label){
            Console.WriteLine("Alarm set for time" + time +"with label"+ label);
        }
        //Abstraction
        public void makeACall(){
            Console.WriteLine("Make a call using the smart watch");
        }
    }
}