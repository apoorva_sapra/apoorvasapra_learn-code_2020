﻿using System;

namespace assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Quartz quartzWatch = new Quartz();
            quartzWatch.brand="fossil";
            quartzWatch.replaceBatteries();
            SmartWatch smartWatch = new SmartWatch();
            smartWatch.showtime();
            smartWatch.setAlarm("5:00am","Wake up");
        }
    }
}
