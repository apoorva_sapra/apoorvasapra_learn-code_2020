﻿using System;

namespace assignment2
{
    class Program
    {
        private static bool isCondition = true;
        private static IStorage storage;
        static void Main(string[] args)
        {

            while (isCondition)
            {
                displayOptions();
                int userInput = getUserInput();
                performStorageOfData(userInput);
            }
        }
        public static void displayOptions()
        {
            Console.WriteLine("\nPlease Select an option:");
            Console.WriteLine("Press 1 to Store in Database. \nPress 2 to store in File. \nPress 3 to Exit.");

        }
        public static int getUserInput()
        {
            try
            {
                int userInput = Int32.Parse(Console.ReadLine());
                return userInput;
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid user input");
                return 0;
            }
        }

        public static void performStorageOfData(int userInput)
        {
            try
            {
                switch (userInput)
                {
                    case 1:
                        storage = new DatabaseStorage();
                        storage.store();
                        break;
                    case 2:
                        storage = new FileStorage();
                        storage.store();
                        break;
                    case 3:
                        isCondition = false;
                        break;
                    default:
                        throw new Exception("Please enter a correct number from the options available :)");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

        }
    }
}
