using System.Collections.Generic;
using System;
namespace assignment2
{
    public class DatabaseStorage : IStorage
    {
        static List<Student> studentsList = new List<Student>();
        public void store()
        {
            Console.WriteLine("Please Enter your details\n");
            Console.WriteLine("Enter Student id: ");
            int studentId = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter email: ");
            string email = Console.ReadLine();
            Console.WriteLine("Enter phone number: ");
            string phone = Console.ReadLine();
            Console.WriteLine("Enter name of college: ");
            string college = Console.ReadLine();
            Student student = new Student(studentId, name, email, phone, college);
            studentsList.Add(student);
            Console.WriteLine("Details added to the database\n");
            display(studentsList);
        }
        public void display(List<Student> studentsList)
        {
            foreach (Student student in studentsList)
            {
                Console.WriteLine("\nStudent Id: "+student.studentId);
                Console.WriteLine("Name: "+student.name);
                Console.WriteLine("Email: "+student.email);
                Console.WriteLine("Phone number: "+student.phone);
                Console.WriteLine("College Name: "+student.college);   
            }
        }
    }
}