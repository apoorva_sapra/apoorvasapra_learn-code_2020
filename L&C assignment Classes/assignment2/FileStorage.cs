using System.IO;
using System;
namespace assignment2
{
    public class FileStorage:IStorage
    {
        static string path = @"Students.txt";
        
        public void store()
        {
            Console.WriteLine("Please Enter your details\n");
            Console.WriteLine("Enter Student id: ");
            int studentId = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter email: ");
            string email = Console.ReadLine();
            Console.WriteLine("Enter phone number: ");
            string phone = Console.ReadLine();
            Console.WriteLine("Enter name of college: ");
            string college = Console.ReadLine();

            Student student = new Student(studentId, name,email,phone,college);
            try{
            using (var file = File.Exists(path)? File.Open(path, FileMode.Append) : File.Open(path, FileMode.CreateNew))
            using (var stream = new StreamWriter(file))
               {
                stream.WriteLine("Id: "+ student.studentId);
                stream.WriteLine("Name: "+student.name);
                stream.WriteLine("Email: "+student.email);
                stream.WriteLine("Phone: "+student.phone);
                stream.WriteLine("College name: "+student.college);
               }
               Console.WriteLine("Details successfully added to the file Students.txt!");
            }
             catch (FileNotFoundException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch(Exception){
                Console.WriteLine("Some error occured. Please try again.");
            }
           
        }
    }
}