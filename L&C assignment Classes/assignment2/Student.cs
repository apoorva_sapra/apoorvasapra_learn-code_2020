namespace assignment2
{
    public class Student
    {
        public int studentId {get; set;}
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string college { get; set; }

        public Student(int studentId,string name, string email, string phone, string college)
        {
            this.studentId=studentId;
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.college = college;
        }
    }
}