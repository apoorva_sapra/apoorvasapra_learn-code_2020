import Location
from configparser import ConfigParser

config = ConfigParser()
config.read('configuration.ini')

def getPlace():
    userInput = input("Enter place name: ")
    return userInput

def getUrl():
    url = config.get('location','endpoint')
    return url

def getLattitudeLongitude(location,jsonObject):
    lattitude = location.getLatitude(jsonObject)
    longitude = location.getLongitude(jsonObject)
    print("Latitude: ", lattitude)
    print("Lomgitude: ", longitude)

def main():
    place = getPlace() 
    location = Location.Location()
    url = getUrl()
    parameters = location.setParameters(config.get('location','key'), place, config.get('location','format'))

    response = location.getResponse(url,parameters)
    jsonObject = location.getJsonObject(response)

    getLattitudeLongitude(location,jsonObject)

main()