import requests
import json

class Location:
    def setParameters(self,key,queryString,format):
        parameters = {
            'key': key,
            #Querystring to search for
            'q': queryString,
            'format': format
        }
        return parameters

    def getResponse(self,url,parameters):
        try:
            return requests.get(url, params=parameters)
        except Exception:
            print("Connection could not be established!")
            exit(0)
        

    def getJsonObject(self,response):
        return json.loads(response.text)

    def getLatitude(self,jsonObject):
        try:
            return (jsonObject[0]['lat'])
        except:
            print("Data not available! Please check the place name.")
            exit(0)

    def getLongitude(self,jsonObject):
        try:
            return (jsonObject[0]['lon'])
        except:
            print("Longitude data not available!")
            exit(0)
        
