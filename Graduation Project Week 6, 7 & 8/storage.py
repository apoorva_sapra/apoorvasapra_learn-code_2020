from constants import Constants
from datetime import datetime
import mysql.connector

class Storage:
    constants = Constants()
    def storeClientRequestInFile(self,clientSocket,fileName,data):
        try:
            with open(fileName, 'a') as file: 
                currentTime=datetime.now().strftime("%H:%M:%S")
                file.write('Client sent '+data + ' at: ' + currentTime +'\n')
        except EOFError as errorMessage:
            print(errorMessage)

    def connectWithDatabase(self):
        databaseConnection = mysql.connector.connect(
        host=Constants.databaseHost,
        user=Constants.databaseUser,
        password=Constants.databasePassword,
        database=Constants.database
        )
        return databaseConnection

    def getCursor(self,databaseConnection):
        cursor = databaseConnection.cursor()
        return cursor

    def storeClientRequestInDatabase(self,clientName,data): 
        databaseConnection = self.connectWithDatabase()
        cursor = self.getCursor(databaseConnection)
        print("connected")
        isTableExists = self.checkTableExists(cursor,clientName)
        print(isTableExists)
        if not isTableExists:
            self.createClientTable(clientName, cursor)
            self.insertIntoReferenceTable(clientName,cursor,databaseConnection)
        self.insertDataInClientTable(clientName,cursor,data,databaseConnection) 

    def checkTableExists(self, cursor, tablename):
        cursor.execute("""
            SELECT COUNT(*)table_name
            FROM information_schema.tables
            WHERE  = '{0}'
            """.format(tablename.replace('\'', '\'\'')))
        if cursor.fetchone()[0] == 1:
            cursor.close()
            return True
        cursor.close()
        return False

    def insertIntoReferenceTable(self,clientName,cursor,databaseConnection):
        try:
            insertQueryForReference= "INSERT INTO ReferenceTable (ClientName) VALUES (%s)" 
            values=(clientName)
            cursor.execute(insertQueryForReference,values)
        except:
            print("Client information could not be stored in reference table")
        databaseConnection.commit()

    def createClientTable(self, clientName, cursor):
        print(clientName)
        try:
            createTableQuery = "CREATE TABLE " + clientName  + """
                    (SerialNumber INT AUTO_INCREMENT KEY,
                    TimeStamp DATETIME,
                    Data VARCHAR(255))"""
            cursor.execute(createTableQuery)
        except:
            print("Client table could not be created.")        

    
    def insertDataInClientTable(self,clientName,cursor,data,databaseConnection):
        now = datetime.utcnow()
        try:
            insertQuery= "INSERT INTO "+ clientName + "(TimeStamp, Data) VALUES (%s, %s)"          
            values=(now.strftime('%Y-%m-%d %H:%M:%S'),data)
            cursor.execute(insertQuery,values)
        except:
            print("Client requests could not be saved.")
        databaseConnection.commit()


