import socket
from constants import Constants

class Client:    
    constants = Constants()
    host = constants.host
    port = constants.port

    def connectClientToServer(self):
        try:
            clientSocket=socket.socket()
            clientSocket.connect((self.host, self.port))
            print("Connected to server")
        except:
            print("Could not connect to server")
        finally:
                return clientSocket

    def communicateWithServer(self,clientSocket):
        try:        
            while True:
                clientMessage = input("enter message to send: ")
                clientSocket.sendall((clientMessage).encode())
                if clientMessage.lower() == self.constants.terminateConnection:
                    break
                response = clientSocket.recv(self.constants.buffer).decode()
                print(response)
        except:
            print("Connection broken.")


def startClient():
    client=Client()
    clientSocket=client.connectClientToServer()
    client.communicateWithServer(clientSocket)

startClient()


