from constants import Constants
from datetime import datetime

class Storage:
    constants = Constants()
    def storeClientRequestInFile(self,clientSocket,fileName,data):
        try:
            with open(fileName, 'a') as file: 
                currentTime=datetime.now().strftime("%H:%M:%S")
                file.write('Client sent '+data + ' at: ' + currentTime +'\n')
        except EOFError as errorMessage:
            print(errorMessage)